package com.example.library.library.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.library.library.entity.Author;
import com.example.library.library.entity.Book;
import com.example.library.library.entity.Publisher;
import com.example.library.library.repo.BookRepo;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class BookControllerTest {
	
	@MockBean
	private BookRepo bookRepo;

	@Test
	void testFindById() {
		List<Author> authors = new ArrayList<Author>();
		Publisher publisher = new Publisher();
		Book book = new Book(11,"kniga",publisher, authors);
		
		Mockito.when(bookRepo.findById(11)).thenReturn(Optional.of(book));
		assertEquals(bookRepo.findById(11), Optional.of(book));
	}

	@Test
	void testFindAll() {
		Mockito.when(bookRepo.findAll()).thenReturn(List.of(new Book()));
		assertEquals(List.of(new Book()), bookRepo.findAll());
	}

}
