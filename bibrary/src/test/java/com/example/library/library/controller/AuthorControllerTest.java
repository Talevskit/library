package com.example.library.library.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.library.library.entity.Author;
import com.example.library.library.entity.Book;
import com.example.library.library.repo.AuthorRepo;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class AuthorControllerTest {

	
	@MockBean
	private AuthorRepo authorRepo;
	
	@Test
	void testFindAll() {
		Mockito.when(authorRepo.findAll()).thenReturn(List.of(new Author()));
		assertEquals(List.of(new Author()), authorRepo.findAll());
	}
	
	@Test
	void testGetById() {
		List<Book> books = new ArrayList<Book>();
		Author author = new Author(3,"tino","talevski",books);
		
		Mockito.when(authorRepo.findById(3)).thenReturn(Optional.of(author));
		assertEquals(authorRepo.findById(3), Optional.of(author));
	}
	
	@Test
	void testSave() {
		List<Book> books = new ArrayList<Book>();
		Author author = new Author(3,"tino","talevski",books);
		
		Mockito.when(authorRepo.save(author)).thenReturn(author);
		assertEquals(authorRepo.save(author), author);
	}

}
