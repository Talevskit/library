package com.example.library.library.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.library.library.entity.Author;
import com.example.library.library.entity.Book;
import com.example.library.library.entity.Publisher;
import com.example.library.library.repo.PublisherRepo;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class PublisherControllerTest {
	
	@MockBean
	private PublisherRepo publisherRepo;

	@Test
	void testCreate() {
		List<Book> books = new ArrayList<Book>();
		Publisher publisher = new Publisher(6,"tino","talevski",books);
		
		Mockito.when(publisherRepo.save(publisher)).thenReturn(publisher);
		assertEquals(publisherRepo.save(publisher), publisher);
	}

	@Test
	void testFindById() {
		List<Book> books = new ArrayList<Book>();
		Publisher publisher = new Publisher(6,"tino","talevski",books);
		
		Mockito.when(publisherRepo.findById(6)).thenReturn(Optional.of(publisher));
		assertEquals(publisherRepo.findById(6), Optional.of(publisher));
	}

	@Test
	void testGetAll() {
		Mockito.when(publisherRepo.findAll()).thenReturn(List.of(new Publisher()));
		assertEquals(List.of(new Publisher()), publisherRepo.findAll());	}

}
