package com.example.library.library.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.library.library.entity.Book;

@Repository
public interface BookRepo extends CrudRepository<Book, Integer>{
	
}
