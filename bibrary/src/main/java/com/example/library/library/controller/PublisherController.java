package com.example.library.library.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.library.library.entity.Publisher;
import com.example.library.library.entity.PublisherDTO;
import com.example.library.library.service.Impl.PublisherService;

@RestController
@RequestMapping("/api/publishers")
@CrossOrigin(origins = "http://localhost:3000")
public class PublisherController {

	@Autowired
	private PublisherService publisherService;

	@PostMapping
	public Publisher create(@RequestBody Publisher publisher) throws Exception {
		return publisherService.create(publisher);
	}

	@GetMapping("/{id}")
	public Optional<Publisher> findById(@PathVariable("id") int id) throws Exception {
		return publisherService.findById(id);
	}
	
	@GetMapping
	public List<PublisherDTO> getAll(){
		return publisherService.getAll();
	}

	@DeleteMapping("/{id}")
	public boolean delete(@PathVariable("id") int id) throws Exception {
		publisherService.deleteById(id);
		return true;
	}

	@PutMapping("/{id}")
	public Publisher update(@RequestBody Publisher publisher, @PathVariable("id") int id) throws Exception {
		return publisherService.update(publisher, id);

	}

}
