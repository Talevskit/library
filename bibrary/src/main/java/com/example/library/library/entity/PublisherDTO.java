package com.example.library.library.entity;

import java.util.List;

import lombok.Data;
@Data
public class PublisherDTO {
	
	private Publisher publisher;
	private List<Book> books;

}
