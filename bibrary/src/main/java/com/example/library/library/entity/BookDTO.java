package com.example.library.library.entity;

import java.util.List;

import lombok.Data;
@Data
public class BookDTO {
	
	private String title;
	private List<Integer> ids;

}
