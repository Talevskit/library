package com.example.library.library.entity;

import java.util.List;

import lombok.Data;
@Data
public class AuthorDTO {
	
	private Author author;
	private List<Book> books;

}
