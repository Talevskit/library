package com.example.library.library.service.Impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.library.library.entity.Author;
import com.example.library.library.entity.Book;
import com.example.library.library.entity.BookDTO;
import com.example.library.library.entity.Publisher;
import com.example.library.library.exceptions.ApiException;
import com.example.library.library.repo.AuthorRepo;
import com.example.library.library.repo.BookRepo;
import com.example.library.library.repo.PublisherRepo;
import com.example.library.library.service.GenericService;

@Service
public class BookService implements GenericService<Book, Integer> {

	@Autowired
	private BookRepo bookRepo;

	@Autowired
	private AuthorRepo authorRepo;
	
	@Autowired
	private PublisherRepo publisherRepo;

	@Override
	public Optional<Book> findById(Integer id) throws Exception {
		if (bookRepo.findById(id).isEmpty())
			throw new ApiException("Book doesn't exist");
		return bookRepo.findById(id);
	}

	@Override
	public Iterable<Book> findAll() {
		return bookRepo.findAll();
	}

	@Override
	public Book create(Book entity) throws Exception {
		if (entity.getTitle() == null)
			throw new ApiException("Invalid Object");
		entity.setAuthors(entity.getAuthors());
		return bookRepo.save(entity);
	}
	

	public Book newBook(BookDTO dto) {
		if (dto.getTitle() == null)
			throw new ApiException("Invalid Object");
			List<Author> authors = new ArrayList<Author>();
			Book book = new Book();
			for (Integer id : dto.getIds()) {
				if(authorRepo.findById(id) == null)
					throw new ApiException("Author with id "+id+" doesn't exist");
				authors.add(authorRepo.findById(id).get());
		}
			book.setTitle(dto.getTitle());
			book.setAuthors(authors);
		return bookRepo.save(book);
	}

	@Override
	public Book update(Book entity, Integer id) throws Exception {
		if (bookRepo.findById(id).isEmpty())
			throw new ApiException("Book doesn't exist");
		Book updated = bookRepo.findById(id).orElseThrow();
		updated.setTitle(entity.getTitle());
		if (entity.getAuthors() == null) {
			
			Iterable<Publisher> publishers = publisherRepo.findAll();
			
			for (Publisher pub : publishers) {
				if(entity.getPublisher().getFirstName().toLowerCase().equals(pub.getFirstName().toLowerCase()) &&
						entity.getPublisher().getLastName().toLowerCase().equals(pub.getLastName().toLowerCase())
						)
					updated.setPublisher(pub);
			}
			
						
		} else {
			updated.getAuthors().addAll(entity.getAuthors());
			updated.setPublisher(entity.getPublisher());
		}
		return bookRepo.save(updated);
	}

	@Override
	public void deleteById(Integer id) throws Exception {
		if (bookRepo.findById(id).isEmpty())
			throw new ApiException("Book doesn't exist");
		bookRepo.deleteById(id);

	}

}
