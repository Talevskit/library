package com.example.library.library.service.Impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.library.library.entity.Author;
import com.example.library.library.entity.AuthorDTO;
import com.example.library.library.entity.Book;
import com.example.library.library.exceptions.ApiException;
import com.example.library.library.repo.AuthorRepo;
import com.example.library.library.repo.BookRepo;
import com.example.library.library.service.GenericService;

@Service
public class AuthorService implements GenericService<Author, Integer> {

	@Autowired
	private AuthorRepo authorRepo;
	@Autowired
	private BookRepo bookRepo;

	@Override
	public Optional<Author> findById(Integer id) throws Exception {
		if (authorRepo.findById(id).isEmpty())
			throw new ApiException("Author doesn't exist");
		return authorRepo.findById(id);
	}

	@Override
	public Iterable<Author> findAll() {
		return authorRepo.findAll();
	}	
	
	
	public AuthorDTO getAuthorBooks(Integer id) {
		Author author = authorRepo.findById(id).get();
		List<Book> books = new ArrayList<Book>();
		AuthorDTO dto = new AuthorDTO();
		for(Book book : (bookRepo.findAll())) {
			if(book.getAuthors().contains(author))
				books.add(book);
		}
		dto.setAuthor(author);
		dto.setBooks(books);
		return dto;
		
	}
	

	@Override
	public Author update(Author entity,Integer id) throws Exception {
		if (authorRepo.findById(id).isEmpty())
			throw new ApiException("Author doesn't exist");
		Author updated = authorRepo.findById(id).orElseThrow();
		updated.setFirstName(entity.getFirstName());
		updated.setLastName(entity.getLastName());
		return authorRepo.save(updated);
	}

	@Override
	public void deleteById(Integer id) throws Exception {
		if (authorRepo.findById(id).isEmpty())
			throw new ApiException("Author doesn't exist");
		authorRepo.deleteById(id);
	}

	@Override
	public Author create(Author entity) throws Exception {
		if (entity.getFirstName() == null || entity.getLastName() == null)
			throw new ApiException("Invalid Object");
		return authorRepo.save(entity);
	}
	

}
