package com.example.library.library.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.library.library.entity.Book;
import com.example.library.library.entity.BookDTO;
import com.example.library.library.service.Impl.BookService;

@RestController
@RequestMapping("/api/books")
@CrossOrigin(origins = "http://localhost:3000")
public class BookController {
	
	@Autowired
	private BookService bookService;
	
	
	@PostMapping
	public Book newBookOldUsers(@RequestBody BookDTO dto) throws Exception {
		return bookService.newBook(dto);
	}

	@GetMapping("/{id}")
	public Optional<Book> findById(@PathVariable("id") int id) throws Exception {
		return bookService.findById(id);
	}

	@GetMapping
	public Iterable<Book> findAll() {
		return bookService.findAll();
	}

	@DeleteMapping("/{id}")
	public boolean delete(@PathVariable("id") int id) throws Exception {
		bookService.deleteById(id);
		return true;
	}

	@PutMapping("/{id}")
	public Book update(@RequestBody Book book, @PathVariable("id") int id) throws Exception {
		return bookService.update(book, id);

	}
	
	

}
