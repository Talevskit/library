package com.example.library.library.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.library.library.entity.Author;
import com.example.library.library.entity.AuthorDTO;
import com.example.library.library.service.Impl.AuthorService;

@RestController
@RequestMapping("/api/authors")
@CrossOrigin(origins = "http://localhost:3000")
public class AuthorController {

	@Autowired
	private AuthorService authorService;

	@PostMapping
	public Author create(@RequestBody Author author) throws Exception {
		return authorService.create(author);
	}
	
	@GetMapping("/{id}")
	public AuthorDTO getAuthorBooks(@PathVariable("id") Integer id) {
		return authorService.getAuthorBooks(id);
	}

	@GetMapping
	public Iterable<Author> findAll() {
		return authorService.findAll();
	}

	@DeleteMapping("/{id}")
	public boolean delete(@PathVariable("id") int id) throws Exception {
		authorService.deleteById(id);
		return true;
	}

	@PutMapping("/{id}")
	public Author update(@RequestBody Author author, @PathVariable("id") int id) throws Exception {
		return authorService.update(author, id);

	}

}
