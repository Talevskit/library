package com.example.library.library.service.Impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.library.library.entity.Publisher;
import com.example.library.library.entity.PublisherDTO;
import com.example.library.library.exceptions.ApiException;
import com.example.library.library.repo.PublisherRepo;
import com.example.library.library.service.GenericService;

@Service
public class PublisherService implements GenericService<Publisher, Integer>{
	
	@Autowired
	private PublisherRepo publisherRepo;

	@Override
	public Optional<Publisher> findById(Integer id) throws Exception {
		if (publisherRepo.findById(id).isEmpty())
			throw new ApiException("Publisher doesn't exist");
		return publisherRepo.findById(id);
	}


	@Override
	public Iterable<Publisher> findAll() {
		return publisherRepo.findAll();
	}
	
	public List<PublisherDTO> getAll(){
		List<PublisherDTO> publishers = new ArrayList<PublisherDTO>();
		for(Publisher pubs : (publisherRepo.findAll())){
			PublisherDTO pub = new PublisherDTO();
			pub.setPublisher(pubs);
			pub.setBooks(pubs.getBooks());
			publishers.add(pub);
		}
		return publishers;
	}

	@Override
	public Publisher create(Publisher entity) throws Exception {
		if(entity.getFirstName() == null || entity.getLastName() == null)
			throw new ApiException("Invalid object");
		return publisherRepo.save(entity);
	}

	@Override
	public Publisher update(Publisher entity, Integer id) throws Exception {
		if (publisherRepo.findById(id).isEmpty())
			throw new ApiException("Publisher doesn't exist");
		Publisher updated = publisherRepo.findById(id).orElseThrow();
		updated.setFirstName(entity.getFirstName());
		updated.setLastName(entity.getLastName());
		publisherRepo.save(updated);
		return updated;
	}

	@Override
	public void deleteById(Integer id) throws Exception {
		if (publisherRepo.findById(id).isEmpty())
			throw new ApiException("Publisher doesn't exist");
		publisherRepo.deleteById(id);
		
	}

	

}
