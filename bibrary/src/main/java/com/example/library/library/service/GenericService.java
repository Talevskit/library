package com.example.library.library.service;

import java.util.Optional;



public interface GenericService<T,ID> {

	public Optional<T> findById(ID id) throws Exception;
	public Iterable<T> findAll();
	public T create(T entity) throws Exception;
	public T update(T entity, ID id) throws Exception;
	public void deleteById(ID id) throws Exception;
	
}
