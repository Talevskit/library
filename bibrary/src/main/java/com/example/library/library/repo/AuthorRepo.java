package com.example.library.library.repo;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.library.library.entity.Author;

@Repository
public interface AuthorRepo extends CrudRepository<Author, Integer>{
	
}
