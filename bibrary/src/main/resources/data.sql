insert into authors (id, first_name, last_name) values (1,'first','author');
insert into authors (id, first_name, last_name) values (2,'second','authr');
insert into authors (id, first_name, last_name) values (3,'third','auth');
insert into authors (id, first_name, last_name) values (4,'fourth','author');

insert into publishers (id, first_name, last_name) values (5,'first','publisher');
insert into publishers (id, first_name, last_name) values (6,'second','pblishr');
insert into publishers (id, first_name, last_name) values (7,'third','pub');
insert into publishers (id, first_name, last_name) values (8,'fourth','publisher');

insert into books (id, title, publisher_id) values (9, 'firstBook',5);
insert into books (id, title, publisher_id) values (10, 'secondBook',6);
insert into books (id, title) values (11, 'thirdBook');
insert into books (id, title) values (12, 'fourthBook');

insert into books_authors (book_id, author_id) values (9,2);
insert into books_authors (book_id, author_id) values (10,2);
insert into books_authors (book_id, author_id) values (10,4);
insert into books_authors (book_id, author_id) values (11,1);
insert into books_authors (book_id, author_id) values (11,4);
insert into books_authors (book_id, author_id) values (12,2);
insert into books_authors (book_id, author_id) values (12,1);