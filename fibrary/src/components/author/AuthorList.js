import axios from "../service/AxiosService";
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

const AuthorList = () => {
  let history = useHistory();

  const [authors, setAuthors] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    axios.get("/authors").then((res) => {
      console.log(res.data);
      setAuthors(res.data);
      setLoading(false);
    });
  }, []);

  return (
    <div>
      <h2>Authors:</h2>
      <div className="row">
        <table className="table table-hover table-striped table-bordered table-dark">
          <thead>
            <tr>
              <th>First name</th>
              <th>Last name</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {loading && <div>Loading...</div>}
            {authors.map((auth) => (
              <tr key={auth.id}>
                <td>{auth.firstName}</td>
                <td>{auth.lastName}</td>
                <td>
                  <button
                    className="btn btn-info"
                    onClick={() => history.push(`author/${auth.id}`)}
                  >
                    Details
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default AuthorList;
