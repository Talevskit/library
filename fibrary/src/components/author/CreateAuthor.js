import React, { useState } from "react";
import axios from "../service/AxiosService";
import { useHistory } from "react-router-dom";

const CreateAuthor = () => {
  let history = useHistory();

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    var author = {
      firstName: firstName,
      lastName: lastName,
    };
    axios.post("/authors", author);
    history.push("/authors");
  };

  return (
    <div className="container col-6">
      <h3 className="mb-5">Create new author</h3>
      <form onSubmit={handleSubmit}>
        <h5>First name</h5>
        <input
          className="form-control mb-4"
          type="text"
          onChange={(e) => setFirstName(e.target.value)}
        ></input>
        <h5>Last name</h5>
        <input
          className="form-control mb-4"
          type="text"
          onChange={(e) => setLastName(e.target.value)}
        ></input>
        <input type="submit" className="btn btn-secondary mt-4" />
      </form>
    </div>
  );
};

export default CreateAuthor;
