import axios from "../service/AxiosService";
import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useHistory } from "react-router-dom";

const AuthorDetails = () => {
  let history = useHistory();

  const { id } = useParams();

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [updatedFirstName, setUpdatedFirstName] = useState("");
  const [updatedLastName, setUpdatedLastName] = useState("");
  const [books, setBooks] = useState([]);

  useEffect(() => {
    axios.get(`/authors/${id}`).then((res) => {
      setFirstName(res.data.author.firstName);
      setLastName(res.data.author.lastName);
      setBooks(res.data.books);
    });
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    var author = {
      firstName: updatedFirstName,
      lastName: updatedLastName,
    };
    axios.put(`/authors/${id}`, author);
    history.push("/authors");
  };

  const handleDelete = () => {
    axios.delete(`/authors/${id}`);
    history.push("/authors");
  };

  return (
    <div className="container col-6">
      <div className="row"></div>

      <div className="container mt-4">
        <div>
          <table className="table table-hover table-striped table-bordered table-dark">
            <thead>
              <tr>
                <th>First name</th>
                <th>Last name</th>
                <th>Books</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{firstName}</td>
                <td>{lastName}</td>
                <td>
                  {books.length == 0 ? (
                    <div>
                      <p>No books yet</p>
                      <button className="btn btn-danger" onClick={handleDelete}>
                        Delete
                      </button>
                    </div>
                  ) : (
                    <div></div>
                  )}
                  {books.map((b) => (
                    <p>{b.title}</p>
                  ))}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div>
          <form onSubmit={handleSubmit}>
            <h2 className="m-4">Edit author</h2>
            <h4>First name</h4>
            <input
              className="form-control mb-4"
              type="text"
              onChange={(e) => setUpdatedFirstName(e.target.value)}
            ></input>
            <h4>Last name</h4>
            <input
              className="form-control mb-4"
              type="text"
              onChange={(e) => setUpdatedLastName(e.target.value)}
            ></input>
            <input type="submit" className="btn btn-secondary mt-4" />
          </form>
        </div>
      </div>
    </div>
  );
};

export default AuthorDetails;
