import React, { useState, useEffect } from "react";
import axios from "../service/AxiosService";
import { useHistory } from "react-router-dom";

const CreateBook = () => {
  let history = useHistory();

  const [authors, setAuthors] = useState([]);
  const [selectedAuthors, setSelectedAuthors] = useState([]);
  const [title, setTitle] = useState("");
  const [chosen, setChosen] = useState([]);
  const [authorArray, setAuthorArray] = useState([]);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");

  useEffect(() => {
    axios.get("/authors").then((res) => {
      setAuthors(res.data);
    });
  }, []);

  const getAuthor = (id) => {
    axios.get(`/authors/${id}`).then((res) => {
      setFirstName(res.data.author.firstName);
      setLastName(res.data.author.lastName);
    });
  };

  const addAuthor = (e) => {
    e.preventDefault();
    setAuthorArray([...authorArray, chosen]);
    getAuthor(chosen);
    setSelectedAuthors([...selectedAuthors, firstName + " " + lastName]);
  };

  const createBook = (e) => {
    e.preventDefault();
    var book = {
      title: title,
      ids: authorArray,
    };
    axios.post(`/books`, book);
    history.push("/books");
  };

  return (
    <div className="container" style={{ width: "500px" }}>
      <div className="p-3">
        <h4>Title</h4>
        <input
          type="text"
          placeholder="Title"
          className="form-control"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        ></input>
      </div>
      <div>
        <h4>Selected Authors</h4>
        <h5>{[...selectedAuthors, firstName + " " + lastName] + " "}</h5>
      </div>
      <div className="p-5">
        <h4>Author</h4>
        <form onSubmit={addAuthor}>
          <div className="row p-3">
            <select
              className="custom-select col-9"
              onChange={(e) => {
                setChosen(e.target.value);
              }}
            >
              <option selected disabled hidden>
                Choose author
              </option>
              {authors.map((author) => (
                <option default key={author.id} value={author.id}>
                  {author.firstName + " " + author.lastName}
                </option>
              ))}
            </select>
            <button className="col-2 btn btn-secondary ml-2" type="submit">
              Add
            </button>
          </div>
        </form>
      </div>
      <button className="col-11 btn btn-primary" onClick={createBook}>
        Submit
      </button>
      <div style={{ position: "absolute", bottom: 60, right: 10 }}>
        <p>
          <strong>Add author</strong>
        </p>
        <button
          style={{
            width: "70px",
            height: "70px",
            backgroundColor: "#ff4d4d",
            borderRadius: "50%",
            border: "none",
            fontSize: "50px",
            textAlign: "center",
          }}
          onClick={() => history.push("/author")}
        >
          +
        </button>
      </div>
    </div>
  );
};

export default CreateBook;
