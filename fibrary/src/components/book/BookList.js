import axios from "../service/AxiosService";
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import PublisherDropdown from "../publisher/PublisherDropdown";

const BookList = () => {
  let history = useHistory();

  const [books, setBooks] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    axios.get("/books").then((res) => {
      setBooks(res.data);
      console.log(res.data);
      setLoading(false);
    });
  }, []);

  const handleDelete = (id) => {
    axios.delete(`/books/${id}`);
    setBooks(books.filter((book) => id !== book.id));
  };

  return (
    <div>
      <div className="row">
        <h3 className="col-12">Books:</h3>
        <div className="col-12">
          <button
            className="btn btn-primary col-2 m-2 float-right"
            onClick={() => {
              history.push("/book");
            }}
          >
            Add Book
          </button>
          <button
            className="btn btn-primary col-2 m-2 float-right"
            onClick={() => {
              history.push("/publisher");
            }}
          >
            Add publisher
          </button>
        </div>
      </div>
      <div className="row">
        <table className="table table-hover table-striped table-bordered table-dark">
          <thead>
            <tr>
              <th>Title</th>
              <th>Authors</th>
              <th>Publisher</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {loading && <div>Loading...</div>}
            {books.map((book) => (
              <tr key={book.id}>
                <td>{book.title}</td>
                <td>
                  {book.authors.map((author) => (
                    <p>{author.firstName + " " + author.lastName}</p>
                  ))}
                </td>
                <td>
                  {book.publisher &&
                    book.publisher.firstName + " " + book.publisher.lastName}
                </td>
                <td style={{ width: "370px" }}>
                  <div>
                    <button
                      className="btn btn-danger m-1 col-3"
                      onClick={() => handleDelete(book.id)}
                    >
                      Delete
                    </button>
                    {!book.publisher && (
                      <PublisherDropdown book={book} className="col-3" />
                    )}
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default BookList;
