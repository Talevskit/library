import React, { useState } from "react";
import axios from "../service/AxiosService";
import { useHistory } from "react-router-dom";

const CreatePublisher = () => {
  let history = useHistory();

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    var publisher = {
      firstName: firstName,
      lastName: lastName,
    };
    axios.post("/publishers", publisher);
    history.push("/books");
  };

  return (
    <div className="container col-6">
      <h3 className="mb-5">Create new publisher</h3>
      <form onSubmit={handleSubmit}>
        <h4>First name</h4>
        <input
          className="form-control mb-4"
          type="text"
          onChange={(e) => setFirstName(e.target.value)}
        ></input>
        <h4>Last name</h4>
        <input
          className="form-control mb-4"
          type="text"
          onChange={(e) => setLastName(e.target.value)}
        ></input>
        <input type="submit" className="btn btn-secondary mt-4" />
      </form>
    </div>
  );
};

export default CreatePublisher;
