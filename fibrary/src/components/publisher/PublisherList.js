import axios from "../service/AxiosService";
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

const PublisherList = () => {
  let history = useHistory();

  const [publishers, setPublishers] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    axios.get("/publishers").then((res) => {
      setPublishers(res.data);
      console.log(res.data);
      setLoading(false);
    });
  }, []);

  const handleDelete = (id) => {
    axios.delete(`/publishers/${id}`);
    setPublishers(publishers.filter((publisher) => id !== publisher.id));
  };

  return (
    <div>
      <h2>Publishers:</h2>
      <div className="row">
        <table className="table table-hover table-striped table-bordered table-dark">
          <thead>
            <tr>
              <th>First name</th>
              <th>Last name</th>
              <th>Books</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {loading && <div>Loading...</div>}
            {publishers.map((pub) => (
              <tr key={pub.publisher.id}>
                <td>{pub.publisher.firstName}</td>
                <td>{pub.publisher.lastName}</td>
                <td>
                  {pub.books.map((b) => (
                    <p>{b.title}</p>
                  ))}
                </td>
                <td>
                  {pub.books.length == 0 ? (
                    <button
                      className="btn btn-danger mr-2"
                      onClick={() => handleDelete(pub.publisher.id)}
                    >
                      Delete
                    </button>
                  ) : (
                    <div></div>
                  )}
                  <button
                    className="btn btn-primary"
                    onClick={() =>
                      history.push(`publisher/${pub.publisher.id}`)
                    }
                  >
                    Edit
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default PublisherList;
