import axios from "../service/AxiosService";
import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useHistory } from "react-router-dom";

const EditPublisher = () => {
  let history = useHistory();

  const { id } = useParams();

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [updatedFirstName, setUpdatedFirstName] = useState("");
  const [updatedLastName, setUpdatedLastName] = useState("");

  useEffect(() => {
    axios.get(`/publishers/${id}`).then((res) => {
      setFirstName(res.data.firstName);
      setLastName(res.data.lastName);
      console.log(res.data);
    });
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    var publisher = {
      firstName: updatedFirstName,
      lastName: updatedLastName,
    };
    axios.put(`/publishers/${id}`, publisher);
    history.push("/publishers");
  };

  return (
    <div className="container">
      <div>
        <h1>Edit publisher</h1>
        <h4 className="mt-5">First name - {firstName}</h4>
        <h4>Last name - {lastName}</h4>
      </div>
      <div className="container col-6 mt-5">
        <form onSubmit={handleSubmit}>
          <h4>First name</h4>
          <input
            className="form-control mb-4"
            type="text"
            onChange={(e) => setUpdatedFirstName(e.target.value)}
          ></input>
          <h4>Last name</h4>
          <input
            className="form-control mb-4"
            type="text"
            onChange={(e) => setUpdatedLastName(e.target.value)}
          ></input>
          <input type="submit" className="btn btn-secondary mt-4" />
        </form>
      </div>
    </div>
  );
};

export default EditPublisher;
