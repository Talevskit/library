import React, { useState, useEffect } from "react";
import axios from "../service/AxiosService";
import { useHistory } from "react-router-dom";

const PublisherDropdown = ({ book }) => {
  let history = useHistory();

  const [publishers, setPublishers] = useState([]);
  const [publisher, setPublisher] = useState("");

  useEffect(() => {
    axios.get(`/publishers/`).then((res) => {
      setPublishers(res.data);
      console.log(res.data);
    });
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    let name = publisher.split(" ");
    let updated = {
      title: book.title,
      publisher: {
        firstName: name[0],
        lastName: name[1],
      },
    };
    axios.put(`/books/${book.id}`, updated);
    history.push("/books");
  };

  return (
    <div style={{ width: "100%" }}>
      <form onSubmit={handleSubmit}>
        <div>
          <select
            className="custom-select col-8"
            onChange={(e) => {
              setPublisher(e.target.value);
            }}
          >
            <option selected disabled hidden>
              Choose publisher
            </option>
            {publishers.map((publisher) => (
              <option key={publisher.id}>
                {publisher.publisher.firstName +
                  " " +
                  publisher.publisher.lastName}
              </option>
            ))}
          </select>
          <button className="btn-sm btn btn-secondary ml-3" type="submit">
            Submit
          </button>
        </div>
      </form>
    </div>
  );
};

export default PublisherDropdown;
