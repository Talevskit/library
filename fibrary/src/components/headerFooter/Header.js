import { useHistory } from "react-router-dom";

const Header = () => {
  let history = useHistory();

  return (
    <div>
      <header>
        <nav className="navbar navbar-expand-md navbar-dark bg-dark">
          <div>
            <a className="navbar-brand">Library</a>
          </div>
          <div>
            <button
              style={{ marginRight: "20px" }}
              className="btn btn-secondary"
              onClick={() => history.push("/")}
            >
              Home
            </button>
            <button
              style={{ marginRight: "20px" }}
              className="btn btn-secondary"
              onClick={() => history.push("/books")}
            >
              Books
            </button>
            <button
              style={{ marginRight: "20px" }}
              className="btn btn-secondary"
              onClick={() => history.push("/authors")}
            >
              Authors
            </button>
            <button
              style={{ marginRight: "20px" }}
              className="btn btn-secondary"
              onClick={() => history.push("/publishers")}
            >
              Publishers
            </button>
          </div>
        </nav>
      </header>
    </div>
  );
};

export default Header;
