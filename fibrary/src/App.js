import "./App.css";
import Footer from "./components/headerFooter/Footer";
import Header from "./components/headerFooter/Header";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import WelcomeComponent from "./components/WelcomeComponent";
import AuthorList from "./components/author/AuthorList";
import BookList from "./components/book/BookList";
import AuthorDetails from "./components/author/AuthorDetails";
import CreateBook from "./components/book/CreateBook";
import CreateAuthor from "./components/author/CreateAuthor";
import CreatePublisher from "./components/publisher/CreatePublisher";
import PublisherList from "./components/publisher/PublisherList";
import EditPublisher from "./components/publisher/EditPublisher";

const App = () => {
  return (
    <div className="App">
      <Router>
        <Header />
        <div className="container">
          <Switch>
            <Route exact path="/" component={WelcomeComponent} />
            <Route path="/authors" component={AuthorList} />
            <Route path="/books" component={BookList} />
            <Route path="/publishers" component={PublisherList} />
            <Route path="/author/:id" component={AuthorDetails} />
            <Route path="/publisher/:id" component={EditPublisher} />
            <Route path="/book" component={CreateBook} />
            <Route path="/publisher" component={CreatePublisher} />
            <Route path="/author" component={CreateAuthor} />
          </Switch>
        </div>
        <Footer />
      </Router>
    </div>
  );
};

export default App;
